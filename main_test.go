package main

import (
	"fmt"
	"testing"
)

type Test struct {
	unpacked []Unpacked
}

type Unpacked struct {
	mask string
	ok   string
}

func TestUnpackString(t *testing.T) {
	var test Test

	test.unpacked = []Unpacked{
		{
			mask: `a4bc2d5e`,
			ok:   `aaaabccddddde`,
		},
		{
			mask: `abcd`,
			ok:   `abcd`,
		},
		{
			mask: `45`,
			ok:   `invalid string`,
		},
		{
			mask: `qwe\4\5`,
			ok:   `qwe45`,
		},
		{
			mask: `qwe\45`,
			ok:   `qwe44444`,
		},
		{
			mask: `qwe\\5`,
			ok:   `qwe\\\\\`,
		},
	}

	for _, v := range test.unpacked {
		unpacked, err := UnpackString(v.mask)
		if err != nil {
			if err.Error() != v.ok {
				t.Error(err.Error())
			}
			continue
		}
		if unpacked != v.ok {
			t.Error("Error: ", unpacked, "fail unpacked")
			continue
		}
		fmt.Println(fmt.Sprintf(`mask: %s result: %s`, v.mask, unpacked))
	}
}
