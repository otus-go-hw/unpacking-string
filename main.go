package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	resp, err := UnpackString(`qwe\\5`)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(resp)
}

func isSlash(rune rune) bool {
	return rune == '\\'
}

func UnpackString(s string) (string, error) {
	var result strings.Builder
	var buffer string
	var escaped bool

	r := []rune(s)
	for _, item := range r {
		if unicode.IsDigit(item) {
			if len(buffer) == 0 {
				return "", errors.New(`invalid string`)
			}
			if len(buffer) > 0 && !escaped {
				n, _ := strconv.Atoi(string(item))
				result.WriteString(strings.Repeat(buffer, n))
				buffer = ""
				continue
			}
		}
		if len(buffer) > 0 {
			if isSlash(item) && !escaped {
				escaped = true
				continue
			}
			escaped = false
			result.WriteString(buffer)
			buffer = string(item)
			continue
		}
		buffer = string(item)
	}
	result.WriteString(buffer)

	return result.String(), nil
}
